//
//  InfoViewController.m
//  GoogleSignIn
//
//  Created by Roshani Mahajan on 27/01/16.
//  Copyright © 2016 Roshani Mahajan. All rights reserved.
//

#import "InfoViewController.h"
#import <Google/SignIn.h>

@interface InfoViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *gmailLabel;
@end

@implementation InfoViewController

#pragma mark - App Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* name =    [defaults objectForKey:@"name"];
    _nameLabel.text = name != nil ? name : @"No Value";
    
    NSString* email =    [defaults objectForKey:@"email"];
    _gmailLabel.text = email != nil ? email : @"No Value";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBAction Method
- (IBAction)didTapSignOut:(id)sender {
    [[GIDSignIn sharedInstance] signOut];
    
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Sign Out" message:@"You are signed out successfully" preferredStyle:UIAlertControllerStyleAlert];
    
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertVC dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertVC addAction:action];
    
        [self presentViewController:alertVC animated:YES completion:nil];
}


@end
