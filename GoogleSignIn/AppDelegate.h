//
//  AppDelegate.h
//  GoogleSignIn
//
//  Created by Roshani Mahajan on 24/01/16.
//  Copyright © 2016 Roshani Mahajan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

