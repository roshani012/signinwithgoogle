//
//  ViewController.m
//  GoogleSignIn
//
//  Created by Roshani Mahajan on 24/01/16.
//  Copyright © 2016 Roshani Mahajan. All rights reserved.
//

#import "ViewController.h"
#import "InfoViewController.h"

@interface ViewController ()

@end

#pragma mark - View Lifecycle Methods

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    // Uncomment to automatically sign in the user.
    
    //[[GIDSignIn sharedInstance] signInSilently];
    // Do any additional setup after loading the view, typically from a nib.
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Presentation Methods

-(void)showInfoScreen {
    InfoViewController *infoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoViewController"];
    [self presentViewController:infoVC animated:YES completion:nil];
}

@end
